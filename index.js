'use strict'

exports.Gateway = require('./lib/gateway').Gateway
exports.Service = require('./lib/service').Service
exports.getMethods = require('./lib/util').getMethods
